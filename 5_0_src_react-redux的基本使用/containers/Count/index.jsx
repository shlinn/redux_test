import CountUI from '../../components/Count'
import {connect} from 'react-redux'
import {
    createDecrementAction,
    createIncrementAction,
    createIncrementAsyncAction
} from '../../redux/count_action';

/*
* 1、mapStateToProps函数返回一个对象，
* 2、对象中的key就作为传递给UI组件props的key，value就作为传递给UI组件props的value
* 3、mapStateToProps返回是UI组件的状态
* */
function mapStateToProps(state) {
    return {count: state}
}

/*
* 1、mapDispatchToProps函数返回一个对象，
* 2、对象中的key就作为传递给UI组件props的key，value就作为传递给UI组件props的value
* 3、mapDispatchToProps返回是UI组件操作状态的方法
* */
function mapDispatchToProps(dispatch) {
    return {
        jia: data => dispatch(createIncrementAction(data)),
        jian: data => dispatch(createDecrementAction(data)),
        jiaAsync: (data, time) => dispatch(createIncrementAsyncAction(data, time))
    }
}


// 使用connect()()创建并暴露一个Count的容器组件
export default connect(mapStateToProps, mapDispatchToProps)(CountUI)
