/**
 *  1、该文件为Count组件服务的reducer，reducer本质为一个函数。
 *  2、reducer函数会接到两个参数：之前的状态（prevState），动作对象（action）
 */

const initState = 0

export default function countReducer(prevState=initState, action) {
    // console.log(prevState)
    const {type, data} = action;
    switch (type) {
        case "increment":
            return prevState + data
        case "decrement":
            return prevState - data
        default:
            return prevState
    }
}