// 引入createStore，专门用于创建redux中最为核心的store对象
import {createStore, applyMiddleware} from 'redux'
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk'
import reducer from './reducers';

export default createStore(reducer, composeWithDevTools(applyMiddleware(thunk)))


