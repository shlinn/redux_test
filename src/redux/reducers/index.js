

import countReducer from './reducers/count'
import {combineReducers} from 'redux';

export default combineReducers({
    countReducer
})